let citations = [{"citation" : "Dans la vie on ne fait pas ce que l'on veut mais on est responsable de ce que l'on est.",
                 "auteur" : "Jean-Paul Sartre"}, {
                   "citation" : "La vie, c'est comme une bicyclette, il faut avancer pour ne pas perdre l'équilibre.",
                   "auteur" : "Albert Einstein"}, {
                   "citation" : "Choisissez un travail que vous aimez et vous n'aurez pas à travailler un seul jour de votre vie.",
                   "auteur" : "Confucius"}, {    "citation" : "Etre libre, ce n'est pas seulement se débarrasser de ses chaînes ; c'est vivre d'une façon qui respecte et renforce la liberté des autres.",
                   "auteur" : "Nelson Mandela"}, {
                   "citation" : "L'ennui dans ce monde, c'est que les idiots sont sûrs d'eux et les gens sensés pleins de doutes.",
                   "auteur" : "Bertrand Russell"}, {
                   "citation" : "Les seuls beaux yeux sont ceux qui vous regardent avec tendresse.",
                   "auteur" : "Coco Chanel"}, {        "citation" : "Quand nous sommes heureux, nous sommes toujours bons ; mais quand nous sommes bons, nous ne sommes pas toujours heureux.",
                   "auteur" : "Oscar Wilde"}, {         "citation" : "La paix n'est pas l'absence de guerre, c'est une vertu, un état d'esprit, une volonté de bienveillance, de confiance, de justice.",
                   "auteur" : "Baruch Spinoza"}, {       "citation" : "Vivre est la chose la plus rare. La plupart des gens se contente d'exister.",
                   "auteur" : "Oscar Wilde"}, {         "citation" : "Jamais le soleil ne voit l'ombre.",
                   "auteur" : "Léonard De Vinci"}, {         "citation" : "L'homme idéal à la recherche de la femme idéale : le meilleur moyen de rester célibataire !",
                   "auteur" : "Dominique Blondeau"}];

$("#go").click(function() {
  
  let nb = Math.floor(Math.random() * 10);
  
  citation = citations[nb].citation;
  
  auteur = citations[nb].auteur;
  
document.getElementById("citations").innerHTML = "«" + citation + "»";
  
  document.getElementById("auteur").innerHTML = auteur;

    let a = Math.floor(Math.random() * 256);
    let b = Math.floor(Math.random() * 256);
    let c = Math.floor(Math.random() * 256);
    let color = "rgb(" + a + "," + b + "," + c + ")";
    document.getElementById("bgcolor").style.background = color;
  
});